<?php
/**
 * @file
 * Mapper that handles any fields added by the text_encrypt module
 *
 */

/**
 * Implements hook_feeds_parser_sources_alter().
 */

function text_encrypt_feeds_parser_sources_alter(&$sources, $content_type) {
  drupal_set_message('text_encrypt_feeds_parser_sources_alter');
  if (!empty($content_type)) {
    $fields = field_info_fields();
    foreach ($fields as $field) {
      if ($field['type'] == 'text_encrypt' && isset($field['bundles']['node']) && in_array($content_type, $field['bundles']['node'])) {
        $sources['parent:text_encrypt:' . $field['field_name']] = array(
          'name' => t('Feed node: Text Encryption (nid): @field_name', array('@field_name' => $field['field_name'])),
          'description' => t('Encrypted Text from the parent feed node.'),
          'callback' => 'text_encrypt_feeds_get_source',
        );
      }
    }
  }
  return $sources;
}

/**
 * Callback for mapping parent node references to child node reference values.
 *
 * @param $source
 *   A FeedsSource object.
 * @param $result
 *   FeedsParserResult object.
 * @param $key
 *   The key as defined in the _feeds_parser_sources_alter() hook defined above.
 * @return array
 *   The node ids that the parent node references.
 */

function text_encrypt_feeds_get_source(FeedsSource $source, FeedsParserResult $result, $key) {
  if ($node = node_load($source->feed_nid)) {
    $results = array();
    $field = substr($key, 22);
    if (!empty($node->{$field}[LANGUAGE_NONE])) {
      foreach ($node->{$field}[LANGUAGE_NONE] as $delta => $value) {
        $results[] = $value['nid'];
      }
    }
    return $results;
  }
}

/**
 * Implements hook_feeds_processor_targets_alter().
 *
 * @see FeedsNodeProcessor::getMappingTargets().
 */
function text_encrypt_feeds_processor_targets_alter(&$targets, $entity_type, $bundle_name) {
  $string_types = array(
    'text_encrypt',
    'text_encrypt_long',
    'text_encrypt_with_summary',
  );
  foreach (field_info_instances($entity_type, $bundle_name) as $name => $instance) {
    $info = field_info_field($name);
    unset($callback);
    if (in_array($info['type'], $string_types)) {
      $callback = 'text_encrypt_feeds_set_target_text';
    }
    if (isset($callback)) {
      $targets[$name] = array(
        'name' => check_plain($instance['label']),
        'callback' => $callback,
        'description' => t('The @label field of the node.', array('@label' => $instance['label'])),
      );
    }
  }
}

/**
 * Callback for mapping text fields.
 */
function text_encrypt_feeds_set_target_text($source, $entity, $target, $value) {
  if (!isset($value)) {
    return;
  }

  if (!is_array($value)) {
    $value = array($value);
  }
  _text_encrypt_feeds_set_target($source, $entity, $target, $value, TRUE);
}

/**
 * Helper for mapping.
 *
 * When the callback is invoked, $target contains the name of the field the
 * user has decided to map to and $value contains the value of the feed item
 * element the user has picked as a source.
 *
 * @param $source
 *   A FeedsSource object.
 * @param $entity
 *   The entity to map to.
 * @param $target
 *   The target key on $entity to map to.
 * @param $value
 *   The value to map. MUST be an array.
 * @param $input_format
 *   TRUE if an input format should be applied.
 */
function _text_encrypt_feeds_set_target($source, $entity, $target, $value, $input_format = FALSE) {
  if (empty($value)) {
    return;
  }

  if ($input_format) {
    if (isset($source->importer->processor->config['input_format'])) {
      $format = $source->importer->processor->config['input_format'];
    }
  }

  $info = field_info_field($target);

  // Iterate over all values.
  $i = 0;
  $field = isset($entity->$target) ? $entity->$target : array();
  foreach ($value as $v) {
    if (!is_array($v) && !is_object($v)) {
      $field[LANGUAGE_NONE][$i]['value'] = $v;
    }
    if ($input_format) {
      if (isset($format)) {
        $field[LANGUAGE_NONE][$i]['format'] = $format;
      }
    }
    if ($info['cardinality'] == 1) {
      break;
    }
    $i++;
  }
  $entity->{$target} = $field;
}
